package richtercloud.arquillian.netbeans.debugging.it;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

@RunWith(Arquillian.class)
@RunAsClient
public class FunctionalIT {
    private final static Logger LOGGER = LoggerFactory.getLogger(FunctionalIT.class);

    @Deployment(testable = false)
    public static Archive<?> createDeployment0() throws TransformerException, XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        WebArchive retValue = Maven.configureResolver().workOffline().resolve("richtercloud:arquillian-netbeans-debugging-web:war:1.0-SNAPSHOT")
                .withoutTransitivity().asSingle(WebArchive.class);
        ByteArrayOutputStream archiveContentOutputStream = new ByteArrayOutputStream();
        retValue.writeTo(archiveContentOutputStream, Formatters.VERBOSE);
        LOGGER.info(archiveContentOutputStream.toString());
        return retValue;
    }

    @Drone
    private WebDriver browser;
    @ArquillianResource
    private URL deploymentUrl;
    @FindBy(id = "helloWorldLabel")
    private WebElement helloWorldLabel;

    @Test
    public void testAll() {
        String url = deploymentUrl.toExternalForm()+"/index.xhtml";
        browser.get(url);
        new WebDriverWait(browser, 5).until(ExpectedConditions.visibilityOf(helloWorldLabel));
        LOGGER.debug("test succeeded");
    }
}
